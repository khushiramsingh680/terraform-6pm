resource "google_compute_instance" "vm01" {
  name         = "vm01"
  machine_type = "n2-standard-2"
  zone         = "us-central1-a"

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

provisioner "local-exec" {
   command = "echo ${self.instance_id} >> instance-id.txt"
}
provisioner "file" {
  source = "vm"
  destination = "/tmp/vm"
}

connection {
  user = "tree"
  private_key = file("vm")
  port = 22
  host = self.network_interface.0.access_config.0.nat_ip
  type = "ssh"
}
provisioner "remote-exec" {
  inline = [  
    sh /tmp/vm,
  ]
}

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }

 
  metadata = {
    ssh-keys  = "tree:${file("vm.pub")}"
   
  }
}
