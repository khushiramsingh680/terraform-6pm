



####
 ## .tf they can be any number and terraform will read all files
 ## .terraform :  It stores terraform drivers information
 # .terraform.lock.hcl    it stores the lock 
 # terraform.tfstate  it stores the state files
 # 





 terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.6.0"
    }
  }
  backend "gcs" {
    bucket  = "tf-state-prodenv"
    prefix  = "terraform/state"
    credentials = "tfmember-4081e9c02aef.json"
  }
  }


provider "google" {
   project     = "tfmember"
   region      = "us-central1"
   credentials = file("tfmember-4081e9c02aef.json")
}




